var Kafka = require('node-rdkafka');

var total_bytes = 0;
var total_packets = 0;

function printStats() {
  console.log(`\nBytes: ${total_bytes}\nPackets: ${total_packets}`);
}

var global_config = {
  'group.id': 'counter',
  'metadata.broker.list': '127.0.0.1:9092',
};

var topic_config = {};

var stream = Kafka.KafkaConsumer.createReadStream(global_config, topic_config, {
  topics: ['hightouch']
});

stream.on('data', function(message) {
  //console.log('Got message:', message.value.toString());
  total_bytes += message.size;
  total_packets++;
});

setInterval(() => printStats(), 5000);
